﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ControllerLivesShip : MonoBehaviour
{
    [SerializeField]
    public Image helthImage;
    [SerializeField]
    public Button _buttonRepairShip;

    public static ControllerLivesShip Instance;

    public float GetFillAmont
    {
        get { return helthImage.fillAmount; }
        set { if (helthImage.fillAmount < 1) { helthImage.fillAmount = 1; } }
    }


    private void Awake()
    {
        Instance = this;    
    }
   public void GetHelth(float a)
    {
        helthImage.fillAmount -= a;
    }
    void Start ()
    {
        _buttonRepairShip.onClick.AddListener(RepairShip);

    }
    void RepairShip()
    {
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.RepairWindow);
    }
	
	void Update ()
    {
		
	}
}
