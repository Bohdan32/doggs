﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Ship : MonoBehaviour
{

    private Vector2 _startPositionShip;
    private Vector3 _startPositionRocket;

    [SerializeField]
    public GameObject rocketPrefab;
    public float speed = 20f;
    [SerializeField]
    public Image helthImage;

    public static Ship Instance;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        _startPositionShip = gameObject.transform.localPosition;
        _startPositionRocket = rocketPrefab.transform.localPosition;
    }
    private void OnEnable()
    {
        _startPositionShip = gameObject.transform.localPosition;
        helthImage.fillAmount = 0;
    }

    public void Construct(float localPositionX)
    {
        gameObject.transform.localPosition = new Vector2(localPositionX,gameObject.transform.localPosition.y);
    }

    public float Helth(float a)
    {
         helthImage.fillAmount += a;
        float b = helthImage.fillAmount;
        ControllerLivesShip.Instance.GetHelth(b);
        return b;
        
    }


    void Update()
    {
       

    }
    public void Shoot(Vector3 clickPosition)
    {
        GameObject go = Instantiate(rocketPrefab);
        go.transform.parent = rocketPrefab.transform.parent;
        go.transform.localScale = new Vector3(1, 1, 1);
        go.SetActive(true);
        go.transform.localPosition = new Vector3(_startPositionRocket.x, _startPositionRocket.y, _startPositionRocket.z);
        go.transform.DOMove(new Vector3(clickPosition.x, clickPosition.y, 0), 3f);
        if (go.transform.position.x > 2000f || go.transform.position.y > 2000)
        {
            Destroy(go,1f);
        }

    }



}

