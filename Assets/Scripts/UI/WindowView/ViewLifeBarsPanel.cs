﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewLifeBarsPanel : MonoBehaviour 
{
    public LifeBar[] LifeBar;
    public SpriteBar[] SpriteBar;
    private float width;

    public void Construct()
    {
        int i = 0;
        foreach (LifeBar bar in LifeBar)
        {
            width = gameObject.GetComponent<RectTransform>().rect.width;
           // bar.Construct(width * 0.2f + i * width * 0.3f - width / 2);
            i++;
        }

        int j = 0;
        foreach (SpriteBar f in SpriteBar)
        {
            width = gameObject.GetComponent<RectTransform>().rect.width;
            f.Construct(width * 0.2f + j * width * 0.3f - width / 2);
            j++;
        }
    }

}
