﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewMenuWindow : WindowViewBase
{

    [SerializeField]
    private Button _buttonStratGame;
    [SerializeField]
    private Text _textCountCoin;
    [SerializeField]
    private Text _textCountLive;
    [SerializeField]
    private ControllerShop _shopController;
    int NumberOpenShip;

    private void Awake()
    {
        //ProfilePlayer.OnEditCoinsCount += EditCountOfCoins;
    }

    void Start ()
    {
        base.Start();
        _buttonStratGame.onClick.AddListener(StartGame);
        buttonClose.onClick.AddListener(Close);
        //EditCountOfCoins(ProfilePlayer.GetCountOfCoins);
      

    }
    private void OnEnable()
    {
        /*_textCountCoin.text = ProfilePlayer.GetCountCoin.ToString();
        _textCountLive.text = ProfilePlayer.GetCountLive.ToString();
        NumberOpenShip = ProfilePlayer.GetOpenShip;*/
        _shopController.GetCheckShip(NumberOpenShip);

    }

    void StartGame()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.Menu, this);
        UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.Field).GetComponent<ViewFieldWindow>().Construct();
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.Field);

    }
    public override void Close()
    {

    }

    void Update () {
		
	}
}
