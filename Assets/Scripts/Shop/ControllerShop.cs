﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerShop : MonoBehaviour {


    [SerializeField]
    private Button _buttonNext;
    [SerializeField]
    private Button _buttonBack;

    [SerializeField]
    private Image _imageBigShip;

    [SerializeField]
    private Image _imageInPanel_1;
    [SerializeField]
    private Image _imageInPanel_2;
    [SerializeField]
    private Image _imageCheckShip;
    [SerializeField]
    private Image _imageInPanel_4;
    [SerializeField]
    private Image _imageInPanel_5;

    [SerializeField]
    private Sprite _imageShipLevel_1;
    [SerializeField]
    private Sprite _imageShipLevel_2;
    [SerializeField]
    private Sprite _imageShipLevel_3;
    [SerializeField]
    private Sprite _imageShipLevel_4;
    [SerializeField]
    private Sprite _imageShipLevel_5;
    [SerializeField]
    private Sprite _imageShipLevel_6;
    [SerializeField]
    private Sprite _imageShipLevel_7;
    [SerializeField]
    private Sprite _imageShipLevel_8;
    [SerializeField]
    private Sprite _imageShipLevel_9;
    [SerializeField]
    private Sprite _imageShipLevel_10;

    int check = 3;

    [SerializeField]
    private Text _textNameShip;

    void Start()
    {
        _buttonNext.onClick.AddListener(NextShip);
        _buttonBack.onClick.AddListener(BackShip);

    }

    void NextShip()
    {
        check++;
        if (check > 10)
        {
            check = 10;
        }
        GetCheckShip(check);

    }
    void BackShip()
    {
        check--;
        if (check < 1)
        {
            check=1;
        }
        GetCheckShip(check);
    }

   public void GetCheckShip(int a)
    {
        switch (a)
        {
   
            case 1:
                _imageCheckShip.sprite = _imageShipLevel_1;
                _imageBigShip.sprite = _imageCheckShip.sprite;
                _imageInPanel_1.gameObject.SetActive(false);
                _imageInPanel_2.gameObject.SetActive(false);
                _imageInPanel_4.sprite = _imageShipLevel_2;
                _imageInPanel_5.sprite = _imageShipLevel_3;
                _textNameShip.text = "This Ship Level 1";
                break;
            case 2:
                _imageCheckShip.sprite = _imageShipLevel_2;
                _imageBigShip.sprite = _imageCheckShip.sprite;
                _imageInPanel_1.gameObject.SetActive(false);
                _imageInPanel_2.gameObject.SetActive(true);
                _imageInPanel_2.sprite = _imageShipLevel_1;
                _imageInPanel_4.sprite = _imageShipLevel_3;
                _imageInPanel_5.sprite = _imageShipLevel_4;
                _textNameShip.text = "This Ship Level 2";
                break;
            case 3:
                _imageCheckShip.sprite = _imageShipLevel_3;
                _imageBigShip.sprite = _imageCheckShip.sprite;
                _imageInPanel_1.gameObject.SetActive(true);
                _imageInPanel_1.sprite = _imageShipLevel_1;
                _imageInPanel_2.sprite = _imageShipLevel_2;
                _imageInPanel_4.sprite = _imageShipLevel_4;
                _imageInPanel_5.sprite = _imageShipLevel_5;
                _textNameShip.text = "This Ship Level 3";
                break;
            case 4:
                _imageCheckShip.sprite = _imageShipLevel_4;
                _imageBigShip.sprite = _imageCheckShip.sprite;
                _imageInPanel_1.sprite = _imageShipLevel_2;
                _imageInPanel_2.sprite = _imageShipLevel_3;
                _imageInPanel_4.sprite = _imageShipLevel_5;
                _imageInPanel_5.sprite = _imageShipLevel_6;
                _textNameShip.text = "This Ship Level 4";
                break;
            case 5:
                _imageCheckShip.sprite = _imageShipLevel_5;
                _imageBigShip.sprite = _imageCheckShip.sprite;
                _imageInPanel_1.sprite = _imageShipLevel_3;
                _imageInPanel_2.sprite = _imageShipLevel_4;
                _imageInPanel_4.sprite = _imageShipLevel_6;
                _imageInPanel_5.sprite = _imageShipLevel_7;
                _textNameShip.text = "This Ship Level 5";
                break;
            case 6:
                _imageCheckShip.sprite = _imageShipLevel_6;
                _imageBigShip.sprite = _imageCheckShip.sprite;
                _imageInPanel_1.sprite = _imageShipLevel_4;
                _imageInPanel_2.sprite = _imageShipLevel_5;
                _imageInPanel_4.sprite = _imageShipLevel_7;
                _imageInPanel_5.sprite = _imageShipLevel_8;
                _textNameShip.text = "This Ship Level 6";
                break;
            case 7:
                _imageCheckShip.sprite = _imageShipLevel_7;
                _imageBigShip.sprite = _imageCheckShip.sprite;
                _imageInPanel_1.sprite = _imageShipLevel_5;
                _imageInPanel_2.sprite = _imageShipLevel_6;
                _imageInPanel_4.sprite = _imageShipLevel_8;
                _imageInPanel_5.sprite = _imageShipLevel_9;
                _textNameShip.text = "This Ship Level 7";
                break;
            case 8:
                _imageCheckShip.sprite = _imageShipLevel_8;
                _imageBigShip.sprite = _imageCheckShip.sprite;
                _imageInPanel_1.sprite = _imageShipLevel_6;
                _imageInPanel_2.sprite = _imageShipLevel_7;
                _imageInPanel_5.gameObject.SetActive(true);
                _imageInPanel_4.sprite = _imageShipLevel_9;
                _imageInPanel_5.sprite = _imageShipLevel_10;
                _textNameShip.text = "This Ship Level 8";

                break;
            case 9:
                _imageCheckShip.sprite = _imageShipLevel_9;
                _imageBigShip.sprite = _imageCheckShip.sprite;
                _imageInPanel_1.sprite = _imageShipLevel_7;
                _imageInPanel_2.sprite = _imageShipLevel_8;
                _imageInPanel_4.gameObject.SetActive(true);
                _imageInPanel_4.sprite = _imageShipLevel_10;
                _imageInPanel_5.gameObject.SetActive(false);
                _textNameShip.text = "This Ship Level 9";
                break;
            case 10:
                _imageCheckShip.sprite = _imageShipLevel_10;
                _imageBigShip.sprite = _imageCheckShip.sprite;
                _imageInPanel_1.sprite = _imageShipLevel_8;
                _imageInPanel_2.sprite = _imageShipLevel_9;
                _imageInPanel_4.gameObject.SetActive(false);
                _imageInPanel_5.gameObject.SetActive(false);
                _textNameShip.text = "This Ship Level 10";
                break;

            default:
                Debug.Log("Problem");
                break;
        }
    }
}
