﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class ScoreBar : MonoBehaviour {

	Slider slider;

	float _maxTime = 0;

	float _currentTime = 0;

    public static ScoreBar Instance;




    void Awake ()
    {
        Instance = this; 
        slider = GetComponent<Slider> (); 
    }
    public void AddNewTime(float a)
    {
        if ((_currentTime += a) > _maxTime)
        {
            _currentTime = _maxTime;
        }

    }
    public void GetTimeScore(float time)
    {
        _currentTime = time;
        _maxTime = _currentTime;
       
    }
    public IEnumerator StopTimer()
    { 
        yield return new WaitForSeconds(0.5f);
        UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.Field).GetComponent<ViewFieldWindow>().Close();
        StopAllCoroutines();
    }

	void Update ()
    {
        _currentTime -= Time.deltaTime;
        slider.value = _currentTime / _maxTime;
       
        if (slider.value > 0)
        {
            
        }
        if (slider.value == 0)
        {
            StartCoroutine(StopTimer()); 
        }

    }
}
