﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewDogsPanel : MonoBehaviour
{
    public Ship[] Dogs;
    private float width; 
    void Start()
    {
        foreach (Ship dog in Dogs)
        {
            
        }
    }
    public void Construct()
    {
        int i = 0;
        foreach (Ship dog in Dogs)
        {
            width = gameObject.GetComponent<RectTransform>().rect.width;
            dog.Construct(width*0.2f +i* width * 0.3f -width / 2);
            i++;
        }
    }
    public void OneDogIsDead()
    {
        foreach (Ship dog in Dogs)
        {
   
        }
        ViewFieldWindow.Instance.GameOver();
    }
}
