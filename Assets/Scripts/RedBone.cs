﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RedBone : MonoBehaviour
{
    public void BoneFunction()
    {
        ViewFieldWindow.Instance.SetCountCoin(-1);
        gameObject.transform.position = ControllerBone.Instance.GetNewPosition();
        ControllerBone.Instance.GetNewBone().transform.position = ControllerBone.Instance.GetNewPosition();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Dog")
        {
            BoneFunction();
        }
    }
}
