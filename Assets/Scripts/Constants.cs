﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants 
{
    public const int TimeOfLife = 20;
    public const float SpeedOfBone = 0.2f;
    public const int TimeOfJumpPuppy = 2;
    public const int TimeOfJumpDog = 2;
    public const int TimeOfJumpBigDog = 1;
    public const int TimeAddByBone = 10;
    public const int TimeTakesByBone = 10;
    public const int StartCountCoin = 10000;
    public const int StartCountLive = 5;

    public static int GetTimeOfJumpByAge(eDogAge age)
    {
        switch (age)
        {
            case eDogAge.Puppy:
                return TimeOfJumpPuppy;
                break;
            case eDogAge.Dog:
                return TimeOfJumpDog;
                    break;
            case eDogAge.BigDog:
                return TimeOfJumpBigDog;
                    break;
            default:
                return 0;
                break;
        }
    }
}

public enum eDogType
{
    FirstType,
    SecondType,
    ThirdType
}

public enum eDogAge
{
    Puppy,
    Dog,
    BigDog
}
