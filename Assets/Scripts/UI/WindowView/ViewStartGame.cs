﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ViewStartGame : WindowViewBase
{
    [SerializeField]
    private Button _btnStartGame;
    [SerializeField]
    private Button _btnExitGame;
    [SerializeField]
    private Button _btnOpenRating;

    void Start()
    {
        base.Start();
        _btnStartGame.onClick.AddListener(StartGame);
        _btnOpenRating.onClick.AddListener(OpenRatingWindow);
        _btnExitGame.onClick.AddListener(ExitGame);

    }

    public override void OnEnable()
    {
        content.GetComponent<CanvasGroup>().DOFade(1, 0.5f);
        gameObject.transform.SetSiblingIndex(gameObject.transform.parent.transform.childCount);

    }

    public override void Close()
    {
        
    }

    public void StartGame()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.StartGame, this);
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.Menu);
    }

    public void OpenRatingWindow()
    {
        //UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.Rating);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
