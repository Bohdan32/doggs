﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;
using System;
using System.Threading;



public class ViewFieldWindow : WindowViewBase, IPointerDownHandler
{

    public static ViewFieldWindow Instance;

    private int _score;

    [SerializeField]
    private Ship _ship;

    private void Awake()
    {
        Instance = this;
    }

    public void SetCountCoin(int a)
    {
        _score += a;
        //_countScore.text = _score.ToString();
    }

    void Start()
    {
        base.Start();
        buttonClose.onClick.AddListener(Close);
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
    }


    public int GetScore()
    {
        return _score;
    }

    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.Field, this);
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.GameOver);
    }

    public void GameOver()
    {
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.GameOver);
        UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.GameOver).GetComponent<ViewGameOverWindow>().Construct(_score);
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.Field, this);
    }

    public void Construct()
    {


    }

    public void OnPointerDown(PointerEventData eventData)
    {
        
        Vector3  click=  eventData.position;
        _ship.Shoot(click);
        

    }
}
