﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewShipContainer : MonoBehaviour
{

    [SerializeField]
    private Ship _ship;
    
    private float width;

    private void OnEnable()
    {
        width = gameObject.GetComponent<RectTransform>().rect.width;
        _ship.Construct(width * 0.01f);
        
    }
    


}
