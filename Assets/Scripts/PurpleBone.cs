﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurpleBone : MonoBehaviour
{
    public void BoneFunction()
    {
        ControllerBone.Instance.SetNewSpeedtBone(30f);
        gameObject.transform.position = ControllerBone.Instance.GetNewPosition();
        ControllerBone.Instance.GetNewBone().transform.position = ControllerBone.Instance.GetNewPosition();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Dog")
        {
            BoneFunction();
        }
    }
}
