﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewWindowRepairShip : WindowViewBase
{
    [SerializeField]
    private Button _buttonRepairShip;
    [SerializeField]
    private Text _textCountCoin;
    float maxPriceRepair = 1200;
    void Start ()
    {
        buttonClose.onClick.AddListener(Close);
        _buttonRepairShip.onClick.AddListener(RepairShip);

    }
    private void OnEnable()
    {
        _textCountCoin.text = CalculatorRepair(ControllerLivesShip.Instance.GetFillAmont).ToString();
    }
    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.RepairWindow,this);
    }
    void RepairShip()
    {
        _textCountCoin.text = 0.ToString();
        ControllerLivesShip.Instance.GetFillAmont = 1;
    }

    float CalculatorRepair(float a)
    {
        float b = maxPriceRepair -(maxPriceRepair * a);
        return b;
    }

    void Update ()
    {
		
	}
}
