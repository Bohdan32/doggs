﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ControllerBone : MonoBehaviour
{
    public GameObject[] _arrayBone = new GameObject[3];

    private GameObject[] _arrayhole = new GameObject[6];

    [SerializeField]
    private RectTransform _panelHolesLeft;
    [SerializeField]
    private RectTransform _panelHolesRight;
    [SerializeField]
    private GameObject _holeLeft;
    [SerializeField]
    private GameObject _holeRight;
    [SerializeField]
    private GameObject _ship;
    private bool IsMoveRight;
   

    private float _speedBone = Constants.SpeedOfBone;

    private int whoMove;

    public static ControllerBone Instance;

    private float widhtBone;

    private void Awake()
    {
        Instance = this;

        FirstPosition();
    }



    public void SetNewSpeedtBone(float speed)
    {
        _speedBone = speed;
    }

    private void OnEnable()
    {
        Debug.Log("Ship pos" + _ship.transform.localPosition);
        _arrayBone[0].transform.position = GetNewPosition();
        _arrayBone[1].transform.position = GetNewPosition();
        _arrayBone[2].transform.position = GetNewPosition();
        whoMove = 0;
        _arrayBone[whoMove].transform.position = _arrayhole[3].transform.position;
    }

    public void FirstPosition()
    {
        float height = _panelHolesLeft.rect.height;

        for (int i = 0; i < 3; i++)
        {
            GameObject go = Instantiate(_holeLeft);
            go.transform.parent = _holeLeft.transform.parent;
            go.transform.localScale = new Vector3(1, 1, 1);
            go.SetActive(true);
            go.transform.localPosition = new Vector3(0, _panelHolesLeft.rect.height / 8 + _panelHolesLeft.rect.height * i / 4 - _panelHolesLeft.rect.height / 4, 0);
            _arrayhole[i] = go;
            GameObject go1 = Instantiate(_holeRight);
            go1.transform.parent = _holeRight.transform.parent;
            go1.transform.localScale = new Vector3(1, 1, 1);
            go1.SetActive(true);
            go1.transform.localPosition = new Vector3(0, _panelHolesLeft.rect.height / 8 + _panelHolesLeft.rect.height * i / 4 - _panelHolesLeft.rect.height / 4, 0);//+ i* Vector2.up * height / 4;
            _arrayhole[_arrayhole.Length -i-1] = go1;
        }
    }


    public GameObject GetNewBone()
    {   
        whoMove = Random.Range(0, _arrayBone.Length);
        return _arrayBone[whoMove];
    }

    public Vector3 GetNewPosition()
    {
        
        int r = Random.Range(0, _arrayhole.Length);
        if (r < 3)
        {
            IsMoveRight = true;
            return  _arrayhole[r].transform.position + Vector3.left*2;
        }
        else
        {
            IsMoveRight = false;
            return  _arrayhole[r].transform.position+ Vector3.right*2;
        }
    }

    private void Update()
    {
       
        if (IsMoveRight)
        {
            _arrayBone[whoMove].transform.Translate(Vector2.right * _speedBone);
            if (_arrayBone[whoMove].transform.position.x > _arrayhole[_arrayhole.Length - 1].transform.position.x + 2)
            {
               GetNewBone();
                _arrayBone[whoMove].transform.position = GetNewPosition();
            }

        }
        else
        {
            _arrayBone[whoMove].transform.Translate(Vector2.left * _speedBone);
            if (_arrayBone[whoMove].transform.position.x < _arrayhole[0].transform.position.x  - 2)
            {
                GetNewBone();
                _arrayBone[whoMove].transform.position = GetNewPosition();
            }
        }
    }
}

