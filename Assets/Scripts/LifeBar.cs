﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class LifeBar : MonoBehaviour
{
    /*
    [SerializeField]
    private Image _imgLiveBar;
    [SerializeField]
    private Dog _dog;
    [SerializeField]
    private GameObject _warningGameObject;

    private bool DogIsDead = false;

    void Awake()
    {
       // gameObject.transform.position = new Vector2 (_dog.gameObject.transform.position.x,gameObject.transform.position.y);
    }

    public void Construct(float localPositionX)
    {
        gameObject.transform.localPosition = new Vector2(localPositionX, gameObject.transform.localPosition.y);
        _imgLiveBar.fillAmount = 1;
        DogIsDead = false;
        _warningGameObject.SetActive(false);
    }

    void Update()
    {
        if (!DogIsDead)
        {
            if (!_dog.IsDead)
            {
                float time = _dog.GetLiveTime();
                _imgLiveBar.fillAmount = time / Constants.TimeOfLife;
                if (time > Constants.TimeOfLife * 0.1f)
                {
                    _warningGameObject.SetActive(false);
                }
                else
                {
                    _warningGameObject.SetActive(true);
                }
            }
            else
            {
                _imgLiveBar.fillAmount = 0;
                DogIsDead = true;
                _warningGameObject.SetActive(false);
            }
        }

    }
    */
}
