﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ExistingDBScript : MonoBehaviour {

	public Text DebugText;

	// Use this for initialization

	DataService ds;

	void Start () 
	{
		ds = new DataService ("Game_db_2.db");
	
		//ds.CreateDB ();
 	  	var profile =  GetPersons ();

		 ToConsole (profile);	 

	}

	public IEnumerable<ModelProfile> GetPersons()
	{
		return ds._connection.Table<ModelProfile>();
	}
	
	private void ToConsole(IEnumerable<ModelProfile> people){
		foreach (var person in people) {
			ToConsole(person.ToString());
		}
	}

	private void ToConsole(string msg){
		DebugText.text += System.Environment.NewLine + msg;
		Debug.Log (msg);
	}

}
