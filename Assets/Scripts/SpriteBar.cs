﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteBar : MonoBehaviour
{
    [SerializeField]
    private Ship _dog;
    [SerializeField]
    private Image _imgLiveBar;

    public void Construct(float localPositionX)
    {
        gameObject.transform.localPosition = new Vector2(localPositionX, gameObject.transform.localPosition.y);
        _imgLiveBar.fillAmount = 1;
    }
    private void OnEnable()
    {

    }

}
