﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StealBone : MonoBehaviour {

    public void BoneFunction()
    {
        gameObject.transform.localPosition = ControllerBone.Instance.GetNewPosition();
    }

    private void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Dog")
        {
            BoneFunction();
        }
    }
}
