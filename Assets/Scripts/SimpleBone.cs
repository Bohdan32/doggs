﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBone : MonoBehaviour
{
    [SerializeField]
    private Ship _ship;

    public void BoneFunction()
    {
        gameObject.transform.position = ControllerBone.Instance.GetNewPosition();
        ControllerBone.Instance.GetNewBone().transform.position = ControllerBone.Instance.GetNewPosition();

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            BoneFunction();
        }
        if (collision.gameObject.tag == "bomb")
        {
            _ship.Helth(0.1f);
        }
    }
   
}
