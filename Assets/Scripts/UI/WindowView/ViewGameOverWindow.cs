﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewGameOverWindow : WindowViewBase
{
    [SerializeField]
    private Button _buttonRestart;
    [SerializeField]
    private Text _txtScore;

    void Start()
    {
        base.Start();
        buttonClose.onClick.AddListener(Close);
        _buttonRestart.onClick.AddListener(RestartGame);
    }

    public void Construct(int score)
    {
        //ProfilePlayer.CurrentScore = score;
        //_txtScore.text = "You reached" + score.ToString();
    }
    public override void Close()
    {
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.GameOver, this);
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.StartGame);
    }
    public void RestartGame()
    {
        ViewFieldWindow.Instance.Construct();
        ViewFieldWindow.Instance.Construct();
        UnityPoolManager.Instance.Push(LoadWindowManager.eWindowNameInLocation.GameOver, this);
        UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.Field);
    }

}
