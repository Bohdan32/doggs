﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerHole : MonoBehaviour
{
    public static ControllerHole Instance;
    [SerializeField]
    private RectTransform _panelHolesLeft;
    [SerializeField]
    private RectTransform _panelHolesRight;
    [SerializeField]
    private GameObject _holeLeft;
    [SerializeField]
    private GameObject _holeRight;

    private void Awake()
    {
        Instance = this;
    } 
    public Vector3[] FirstPosition()
    {

        Vector3[] array = new Vector3[6];
        float height = _panelHolesLeft.rect.height;
        Debug.Log(_panelHolesLeft.rect.height);
        //Vector2 button = new Vector2(_rightPanel.position.x, _dog.position.y + _dog.rect.height / 2);
        for (int i = 0; i < 3; i++)
        {
            GameObject go = Instantiate(_holeLeft);
            go.transform.parent = _holeLeft.transform.parent;
            go.transform.localScale = new Vector3(1, 1, 1);
            go.SetActive(true);
            go.transform.localPosition = new Vector3(0, _panelHolesLeft.rect.height / 4 + _panelHolesLeft.rect.height*i / 4- _panelHolesLeft.rect.height / 2, 0);
            array[i] = go.transform.position;
            GameObject go1 = Instantiate(_holeRight);
            go1.transform.parent = _holeRight.transform.parent;
            go1.transform.localScale = new Vector3(1, 1, 1);
            go1.SetActive(true);
            go1.transform.localPosition = new Vector3(0, _panelHolesLeft.rect.height / 4 + _panelHolesLeft.rect.height * i / 4 - _panelHolesLeft.rect.height / 2, 0);//+ i* Vector2.up * height / 4;
            array[array.Length - i-1] = go1.transform.position;
        }
        return array;
    }
}
